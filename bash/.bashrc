# Stop folders from being highlighted in terminal
LS_COLORS=$LS_COLORS:'ow=1;34:' ; export LS_COLORS

# Custom prompt statement
PS1="\e[1;35m\w\e[0m#\n>"

# Source git autocomplete
if [ -f /usr/share/bash-completion/completions/git ]
then
    source /usr/share/bash-completion/completions/git
fi

alias svim='XDG_DATA_HOME=~ XDG_CONFIG_HOME=~/.dotfiles/nvim/simple-config/ nvim'
export RIPGREP_CONFIG_PATH=~/.dotfiles/ripgrep/.ripgreprc
alias py='python3 -i ~/.dotfiles/python/startupImports.py'
alias nvim-config='nvim ~/.config/nvim/'
alias cbb='cmake --build build'
alias notes='cd $SCOTT_NOTES'
