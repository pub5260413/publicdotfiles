# Clone Repo
``` bash
git clone git@gitlab.com:super-private/doti-bois.git ~/.dotfiles
```
# Track LOC
A simple config is a happy config
`find . -name "*.lua" -type f -exec wc -l {} +`

# TODO
## Core Install Improvements
- See notes in install.sh

## Neovim Install Improvements
- LSP servers
  - Install specific servers by default
- Treesitter things
  - Install specific parsers by default

### Modifications After Install
- Enable completion suggestions from Python LSP
  - `~/.local/share/nvim/mason/packages/python-lsp-server/venv`
    - `include-system-site-packages = true`

# Known Issues
- Keyboard `s` key stops working in WSL
  - If `.inputrc` file already has `set bell-style none` and `.dotfile/bash/.inputrc` is sourced from that file
  - Fix is to remove duplicate commands from existing `.inputrc` and source `.dotfile/bash/.inputrc`

# Common Keymaps
## Ctrl
- <C-n>
  - Open file tree
- <C-b>
  - Open code outline
- <C-q>
  - Close command line / python
- <C-w>
  - Delete last word in insert mode

## Leader
- <leader>[1-8]
  - Change current buffer to #1-8
- <leader>d
  - Delete current buffer
- <leader>h
  - Find cpp/header pair
- <leader>c
  - Open command line
- <leader>py
  - Open python
- <leader>tr
  - Send text to repl
