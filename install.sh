#! /usr/bin/bash

# Install gcc / cmake / python3 / node.js / yarn / ripgrep / pandoc / basic utils

# Install and build neovim

# Install and build packer
# git clone --depth 1 https://github.com/wbthomason/packer.nvim\ ~/.local/share/nvim/site/pack/packer/start/packer.nvim

# Disable native nvim parser
if [ -d /usr/local/lib/nvim/parser ]
then
    mv /usr/local/lib/nvim/parser /usr/local/lib/nvim/parser.old
fi

# Link nvim config to .config
if ! [ -L ~/.config/nvim ] && [ -d ~/.config/nvim ]
then
    mv ~/.config/nvim ~/.config/nvim.backup
    ln -s ~/.dotfiles/nvim ~/.config/
elif ! [ -L ~/.config/nvim ]
then
    ln -s ~/.dotfiles/nvim ~/.config/
fi

# Link simple nvim config
if ! [ -d ~/.dotfiles/nvim/simple-config/nvim/lua/scott ]
then
    ln -s ~/.dotfiles/nvim/lua/scott ~/.dotfiles/nvim/simple-config/nvim/lua/scott
    ln -s ~/.dotfiles/nvim/after/ftplugin/REPL.lua ~/.dotfiles/nvim/simple-config/nvim/after/ftplugin/
fi

# Link .inputrc to home or source if file exists
if ! [ -L ~/.inputrc ] && ! [ -f ~/.inputrc ]
then
    ln -s ~/.dotfiles/bash/.inputrc ~
elif [ -f ~/.inputrc ] && ! [ -L ~/.inputrc ] && ! grep -Fxq "source ~/.dotfiles/bash/.inputrc" ~/.inputrc
then
    echo "source ~/.dotfiles/bash/.inputrc" >> ~/.inputrc
fi

# Source .bashrc
if ! grep -Fxq "source ~/.dotfiles/bash/.bashrc" ~/.bashrc
then
    echo "source ~/.dotfiles/bash/.bashrc" >> ~/.bashrc
fi

# Storage for pandoc converted word documents
if ! [ -d ~/pandoc_word ]
then
    mkdir ~/pandoc_word
fi

# Install python packages
# pip install -r ~/.dotfiles/python/requirements.txt
