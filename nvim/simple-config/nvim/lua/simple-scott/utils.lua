local theme = require("simple-scott.theme")

local M = {}

-- Get current mode
function M.get_mode()
    local currentMode = vim.fn.mode()
    if currentMode == 'n' then
        return 'NORMAL', theme.blue
    elseif currentMode == 'i' then
        return 'INSERT', theme.green
    elseif currentMode == 'c' then
        return 'COMMAND', theme.yellow
    elseif currentMode == 't' then
        return 'TERMINAL', theme.purple
    elseif currentMode == 'v' or 'V' or '^V' then
        return 'VISUAL', theme.orange
    else
        return 'DUMBO', theme.blue
    end
end

return M
