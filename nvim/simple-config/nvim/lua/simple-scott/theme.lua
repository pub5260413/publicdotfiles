vim.cmd.colorscheme("habamax")

local M = {}

-- Habamax colorschemes
M.orange    = '#Scott_Orange#'
M.purple    = '#Scott_Purple#'
M.yellow    = '#Scott_Yellow#'
M.blue      = '#Scott_Blue#'
M.green     = '#Scott_Green#'

vim.api.nvim_set_hl(0, 'Scott_Orange', { fg = '#1c1c1c', bg = '#d7875f' })
vim.api.nvim_set_hl(0, 'Scott_Purple', { fg = '#1c1c1c', bg = '#af87af' })
vim.api.nvim_set_hl(0, 'Scott_Yellow', { fg = '#1c1c1c', bg = '#afaf87' })
vim.api.nvim_set_hl(0, 'Scott_Blue', { fg = '#1c1c1c', bg = '#87afd7' })
vim.api.nvim_set_hl(0, 'Scott_Green', { fg = '#1c1c1c', bg = '#87af87' })

return M
