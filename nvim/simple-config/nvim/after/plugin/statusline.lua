local theme = require("simple-scott.theme")
local utils = require("simple-scott.utils")

-- Get current project name
local function project_name()
    if vim.o.columns > 72 then
        return vim.fn.fnamemodify(vim.fn.getcwd(), ':t')
    else
        return ''
    end
end

-- Get current branch name
local function branch_name()
    local branch = vim.fn.system("git branch --show-current 2> /dev/null | tr -d '\n'")
    if branch ~= "" then
        return branch
    else
        return ""
    end
end

-- Get current git status
local function diff_source()
    local gitsigns = vim.b.gitsigns_status_dict
    if gitsigns then
        return {
            added = gitsigns.added,
            modified = gitsigns.changed,
            removed = gitsigns.removed
        }
    end
end

-- Get LSP diagnostics
local function getLSPDiagnostics()
    vim.diagnostic.get(0, { severity = vim.diagnostic.severity.WARN })
end

-- Setup statusline function call
-- Fix this formating, jeezlus FUCK IT"S SOO UGLY
local function setStatusLine()
    local mode, modeColor = utils.get_mode()
    local currentBranch = branch_name()
    local currentDirectory = project_name()
    local currentExtension = vim.bo.filetype

    return string.format(
        "%%%s[%s]%%#Normal#[%s][%s] %%=%%%s%s",
        modeColor,
        mode,
        currentDirectory,
        currentBranch,
        modeColor,
        currentExtension
    )
end


-- Initial setup
vim.opt.statusline = setStatusLine()
vim.opt.laststatus = 3

-- Update statusline on events
vim.api.nvim_create_augroup("ScottStatusLine", { clear = true })
vim.api.nvim_create_autocmd("ModeChanged", {
    callback = function()
        vim.opt.statusline = setStatusLine()
    end,
    group = "ScottStatusLine"
})
