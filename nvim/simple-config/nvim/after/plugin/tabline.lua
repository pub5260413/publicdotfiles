local theme = require("simple-scott.theme")
local utils = require("simple-scott.utils")

local bufArray = {}
local maxBufDisplay

-- Limit max display length
local function bufDisplayLength()
    if vim.o.columns > 100 then
        maxBufDisply = 4
    else
        maxBufDisplay = 8
    end
end

local function colorMe(base, append, color)
    color = "%" .. color
    return base .. color .. " " .. append .. " " .. "%#Normal#|"
end

-- Find buffer names to display
local function bufDisplay(bufferList, buffers, bufName, modeColor, i)
    local currentBufName = vim.fn.expand('%:p')
    -- Add symbol to modified buffer add symbol to modified buffer
    if buffers[i].changed == 1 then
        bufName = bufName .. " +"
    end
    -- Color current buffer based on active mode
    if buffers[i].name == currentBufName then
        bufferList = colorMe(bufferList, bufName, modeColor)
    else
        bufferList = colorMe(bufferList, bufName, "#Normal#")
    end
    return bufferList
end

function setBuffers()
    local tablineBuffers = "%#Normal#|"

    local buffers = vim.fn.getbufinfo({ buflisted = 1 })
    local count = 0
    local _, modeColor = utils.get_mode()

    -- Loop though open buffers
    for i = 1, #buffers do
        -- limit Buffer display to 8
        if count > 8 then
            break
        end

        -- Extract filename from full path
        local bufName = string.match(buffers[i].name, "[^/]+$")
        -- Truncate long name
        if bufName == 20 then
            bufName = bufName .. " +"
        end

        if bufName then
            tablineBuffers = bufDisplay(tablineBuffers, buffers, bufName, modeColor, i)
            -- Map <leader> number key to swap buffers
            count = count + 1
            vim.keymap.set("n", "<leader>" .. count + 0, ":b" .. buffers[i].bufnr .. "<cr>",
                { silent = true, noremap = true })
        end
    end

    -- Set empty tabline space to normal
    tablineBuffers = tablineBuffers .. "%#Normal#"

    -- Unmap keys with no corresponding buffer
    for i = count + 1, 8 do
        vim.keymap.set("n", "<leader>" .. i + 0, "", { silent = true, noremap = true })
    end

    return tablineBuffers
end

-- Initial setup
if setBuffers() then
    vim.opt.tabline = setBuffers()
    vim.opt.showtabline = 2
end

-- Delete buffers and update list
vim.api.nvim_set_keymap("n", "<leader>d", '', {
    callback = function()
        vim.cmd('bp|bd#')
        vim.opt.tabline = setBuffers()
    end,
    silent = true,
    noremap = true
})

-- Set up autocommands to update
vim.api.nvim_create_augroup("ScottTablineLine", { clear = true })
vim.api.nvim_create_autocmd({ "BufAdd", "BufModifiedSet", "ModeChanged", "BufEnter", "BufWinEnter", "BufWritePost" }, {
    callback = function()
        vim.opt.tabline = setBuffers()
    end,
    group = "ScottTablineLine"
})
