-- Initalize active flag
vim.g.netrwActive = false

vim.keymap.set("n", "<c-n>", function()
    vim.cmd("Lex | vertical resize 40 | wincmd =")
    vim.g.netrwActive = not vim.g.netrwActive
    end)

vim.g.netrw_banner = 0
vim.g.netrw_liststyle = 3

-- Remove empty netrw buffers
vim.api.nvim_create_augroup("RemoveNetrwHiddenBuffers", { clear = true })
vim.api.nvim_create_autocmd("FileType", {
    pattern = { 'netrw' },
    callback = function()
        vim.cmd('setlocal bufhidden=wipe')
    end,
    group = "RemoveNetrwHiddenBuffers"
})
