local utils = require("scott.utils")

local function lowBudgetFloaterm(fileName, cmd)
    local fileType = vim.bo.filetype
    if fileType == "netrw" then
        return
    end
    local bufWinNum = #vim.api.nvim_tabpage_list_wins(0)
    utils.NewBufferAtPosition("beta")
    if vim.fn.bufexists(fileName) ~= 0 then
        vim.cmd("b " .. fileName)
        vim.api.nvim_feedkeys(
            vim.api.nvim_replace_termcodes("i", true, false, true)
            , 'x!', true)
    else
        vim.g.lastBuffer = vim.fn.bufnr()
        if bufWinNum == 1 or (bufWinNum == 2 and vim.g.netrwActive) then
            vim.g.lastWinState = 1
        else
            vim.g.lastWinState = 2
        end
        vim.cmd("term")
        vim.cmd("file " .. fileName)
        vim.bo.filetype = "REPL"
        vim.api.nvim_feedkeys(
            vim.api.nvim_replace_termcodes(cmd, true, false, true)
            , 'x!', true)
    end
end

-- Low budget floaterm python
vim.keymap.set("n", "<leader>py", function()
    lowBudgetFloaterm("pyREPL", "ipython3 -i ~/.dotfiles/python/startupImports.py<cr><C-l>")
end, { silent = true, noremap = true })

-- Low budget floaterm bash
vim.keymap.set("n", "<leader>c", function()
    lowBudgetFloaterm("bashREPL", "i")
end, { silent = true, noremap = true })
