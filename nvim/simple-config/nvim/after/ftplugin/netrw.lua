-- Pretty
vim.wo.fillchars = 'eob: '
vim.wo.statuscolumn = ''

-- Remap <Plug> default of NetrwRefresh
vim.keymap.set("n", "<C-l>", "<C-w>l", { buffer = true })
-- Unmap keys with no corresponding buffer
for i = 1, 8 do
    vim.keymap.set("n", "<leader>" .. i+0, "", { buffer = true })
end
