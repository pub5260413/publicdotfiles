local utils = require('scott.utils')

-- Scott
vim.g.mapleader = " "
vim.keymap.set("n", "-", "$")
vim.keymap.set("n", "d-", "d$")
vim.keymap.set("v", "-", "$h")
vim.keymap.set("n", "<CR>", "o<Esc>")
vim.keymap.set("t", "<C-n>", "<C-\\><C-n>")
vim.keymap.set("n", "<leader>d", ":bp|bd#<cr>", { silent = true, noremap = true })

-- Move in insert
vim.keymap.set("i", "<C-l>", "<right>")
vim.keymap.set("i", "<C-h>", "<left>")

-- Primeagin
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

vim.keymap.set("x", "<leader>p", "\"_dP")
vim.keymap.set("v", "<leader>d", "\"_d")
vim.keymap.set("v", "<leader>c", "\"_di")

vim.keymap.set("n", "<leader>y", "\"+y")
vim.keymap.set("v", "<leader>y", "\"+y")
vim.keymap.set("n", "<leader>Y", "\"+Y")

-- NvChad
-- switch between windows
vim.keymap.set("n", "<C-h>", "<C-w>h")
vim.keymap.set("n", "<C-l>", "<C-w>l")
vim.keymap.set("n", "<C-j>", "<C-w>j")
vim.keymap.set("n", "<C-k>", "<C-w>k")

-- Send visual selection to repl
vim.keymap.set("v", "<leader>tr", "y:wincmd l<cr>pi<cr><C-\\><C-n>:wincmd h<cr>")

-- Builtin LSP
vim.keymap.set("n", "'", function() vim.diagnostic.goto_next() end, { silent = true, noremap = true })

-- DIY Ouroborous (pair source and header) w/ linux utitlity find
vim.keymap.set("n", "<leader>h",
    function()
        local result
        local currentDir = vim.uv.cwd()
        local fileName = vim.fn.expand("%:t:r")
        local fileExtension = vim.fn.expand("%:e")
        local fileSearch

        if fileExtension == "c" or fileExtension == "cpp" then
            fileSearch = io.popen(string.format("find %s/ -name \"%s.h\" -o -name \"%s.hpp\"",
                currentDir, fileName, fileName))
        elseif fileExtension == "h" or fileExtension == "hpp" then
            fileSearch = io.popen(string.format("find %s/ -name \"%s.c\" -o -name \"%s.cpp\"",
                currentDir, fileName, fileName))
        end

        if fileSearch and fileSearch ~= '' then
            result = fileSearch:read("*l")
            fileSearch:close()
            if result == nil then
                print("Couldn't find shit")
            else
                if fileExtension == "c" or fileExtension == "cpp" and
                    not string.match(vim.fn.expand('%'), "%NvimTree_") then
                    utils.NewBufferAtPosition(utils.enum.beta)
                    vim.cmd("e " .. result)
                elseif fileExtension == "h" or fileExtension == "hpp" and
                    not string.match(vim.fn.expand('%'), "%NvimTree_") then
                    utils.NewBufferAtPosition(utils.enum.alpha)
                    vim.cmd("e " .. result)
                end
            end
        else
            print("Couldn't find shit")
        end
    end, { silent = true, noremap = true
    })

-- Go to next merge conflict
vim.keymap.set("n", "<leader>.", function()
    local file = vim.fn.expand('%:p')
    local changeList = vim.fn.system([[git diff --check ]] ..
        file .. [[ | awk -F ':' '{print $2}' | sed -ze "$! s/\\n/ /g"]])
    local cursorPos = vim.fn.getcurpos()
    changeList = utils.string2vect(changeList)
    if changeList ~= nil and #changeList > 0 then
        for i = 1, #changeList do
            if changeList[i] > cursorPos[2] then
                vim.cmd(tostring(changeList[i]))
                break
            else
                vim.cmd(tostring(changeList[1]))
            end
        end
    else
        print('No more conflict')
    end
end, { silent = true, noremap = true })
