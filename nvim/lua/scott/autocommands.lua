vim.api.nvim_create_augroup("ScottVimOpen", { clear = true })
vim.api.nvim_create_autocmd("VimEnter", {
    command = [[echo("Hello Scott <3")]],
    group = "ScottVimOpen"
})

-- Toggle cursorline
vim.api.nvim_create_augroup("ToggleCursorLine", { clear = true })
vim.api.nvim_create_autocmd("WinEnter", {
    callback = function()
        vim.opt_local.cursorline = true
    end,
    group = "ToggleCursorLine"
})

vim.api.nvim_create_autocmd("WinLeave", {
    callback = function()
        vim.opt_local.cursorline = false
    end,
    group = "ToggleCursorLine"
})

-- Disable continue comments on new line
vim.api.nvim_create_augroup("VimSet", { clear = true })
vim.api.nvim_create_autocmd("BufWinEnter", {
    callback = function()
        vim.cmd("set formatoptions-=cro")
        vim.opt.numberwidth = 1
    end,
    group = "VimSet"
})

-- Set fancy numberline on new file (not sure why it's set by default)
vim.api.nvim_create_autocmd({ "BufWinEnter", "BufNewFile" }, {
    command = [[lua vim.opt.statuscolumn = '%s%=%{v:relnum?v:relnum:v:lnum} ']],
    group = "VimSet"
})

-- F5 functionality for different filetypes
vim.api.nvim_create_augroup("F5_Functionality", { clear = true })

-- Run cmake from c/cpp files
vim.api.nvim_create_autocmd("BufWinEnter", {
    pattern = { "*.c", "*.cpp", "*.h" },
    callback = function()
        vim.api.nvim_set_keymap("n", "<F5>", '', {
            callback = function()
                if vim.fn.filereadable('CMakeLists.txt') == 1 then
                    vim.cmd("!cmake --build build")
                elseif vim.fn.filereadable('Makefile') == 1 then
                    vim.cmd("make run")
                else
                    print("YOU CAN\'T RUN THAT SHIT")
                end
            end,
            silent = true,
            noremap = true
        })
    end,
    group = "F5_Functionality"
})

-- Make scratch buffer
vim.api.nvim_create_user_command('Scratch', ':enew | setlocal buftype=nofile bufhidden=hide noswapfile | file scratch',
    {})

-- Put time into buffer at cursor
vim.api.nvim_create_user_command('Time', function()
    vim.fn.setreg("q", vim.fn.system([[date -u +"%D @ %H:%M:%S"]]):sub(1, -2), "c")
    vim.fn.feedkeys([["qp"<esc>]])
end, {})

-- Calculate mean of visual selection
vim.api.nvim_create_user_command('Mean', function()
    vim.cmd([['<,'>!awk '{s+=$1}END{print "ave:",s/NR}']])
end, { range = '%' })
