local M = {}

M.enum = {
    alpha = 1,
    beta = 2
}

-- Coppied from habamax
M.palette = {
    black = 'black',
    bg_dim = 'black',
    fg = '#e2e2e3',
    red = '#d75f5f',
    dark_red = '#af5f5f',
    orange = '#d7875f',
    light_orange = '#ffaf5f',
    yellow = '#afaf87',
    green = '#87af87',
    blue = '#87afd7',
    dark_blue = '#3f7aab',
    purple = '#af87af',
    grey = '#7f8490',
}

-- Move to alpha (RH/UP) or beta (LH/DOWN) and execute command
function M.NewBufferAtPosition(pos)
    local splitType = "vsplit"

    if vim.o.columns > 100 then
        if pos == M.enum.alpha then
            pos = "h"
        else
            pos = "l"
        end
    else
        if pos == M.enum.alpha then
            pos = "k"
            splitType = "split"
        else
            pos = "j"
            splitType = "split"
        end
    end

    if #vim.api.nvim_tabpage_list_wins(0) == 2 then
        if vim.fn.bufwinnr("__Tagbar__") == 1 or
            vim.g.netrwActive or
            vim.fn.bufwinnr("NvimTree") == 1 then
            vim.cmd(splitType)
            vim.cmd("wincmd " .. pos)
        else
            vim.cmd("wincmd " .. pos)
        end
    elseif #vim.api.nvim_tabpage_list_wins(0) > 2 then
        vim.cmd("wincmd " .. pos)
    else
        vim.cmd(splitType)
        vim.cmd("wincmd " .. pos)
    end
end

function M.string2vect(s)
    local t = {}
    s:gsub('%-?%d+', function(n) t[#t + 1] = tonumber(n) end)
    return t
end

return M
