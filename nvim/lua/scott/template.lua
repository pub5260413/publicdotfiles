vim.api.nvim_create_augroup("Skeletons", { clear = true })

-- Python shebang
vim.api.nvim_create_autocmd("BufNewFile", {
    pattern = { "*.py" },
    command = "0r ~/.config/nvim/template/skeleton.py",
    group = "Skeletons"
})

-- Bash shebang
vim.api.nvim_create_autocmd("BufNewFile", {
    pattern = { "*.sh" },
    command = "0r ~/.config/nvim/template/skeleton.sh",
    group = "Skeletons"
})

-- Clang-format
vim.api.nvim_create_autocmd("BufNewFile", {
    pattern = { "*.clang-format" },
    command = "0r ~/.config/nvim/template/skeleton.clang-format",
    group = "Skeletons"
})

-- clangd include path
vim.api.nvim_create_autocmd("BufNewFile", {
    pattern = { "*.clangd" },
    command = "0r ~/.config/nvim/template/skeleton.clangd",
    group = "Skeletons"
})

-- pybind11 template
vim.api.nvim_create_autocmd("BufNewFile", {
    pattern = { "*.cpp" },
    callback = function(data)
        if string.match(data.match, "/python/") then
            vim.cmd("0r ~/.config/nvim/template/skeleton.pybind")
        end
    end,
    group = "Skeletons"
})

-- cmake template
vim.api.nvim_create_autocmd("BufNewFile", {
    pattern = { "CMakeLists.txt" },
    callback = function(data)
        if string.match(data.match, "/src/") then
            vim.cmd("0r ~/.config/nvim/template/skeleton.cmake-src")
        elseif string.match(data.match, "/tests/") then
            vim.cmd("0r ~/.config/nvim/template/skeleton.cmake-tests")
        elseif string.match(data.match, "/python/") then
            vim.cmd("0r ~/.config/nvim/template/skeleton.cmake-pybind")
        else
            vim.cmd("0r ~/.config/nvim/template/skeleton.cmake-root")
        end
    end,
    group = "Skeletons"
})
