-- Delete low budget floaterm buffers
vim.keymap.set({ 'n', 't' }, "<C-q>", function()
    vim.cmd("stopi")
    if vim.fn.buflisted(vim.g.lastBuffer) then
        vim.cmd("b" .. vim.g.lastBuffer .. "|bd!#")
    else
        vim.cmd("bp|bd!#")
    end
    if vim.g.lastWinState == 1 then
        vim.cmd("q")
    end
end, { buffer = true })
