-- Don't try to delet nvim tree buffers
vim.keymap.set("n", "<leader>d", "<nop>", { buffer = true })
