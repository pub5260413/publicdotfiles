-- Pretty
vim.wo.fillchars = 'eob: '
vim.wo.statuscolumn = ''

-- Don't try to delete tagbar buffers
vim.keymap.set("n", "<leader>d", "<nop>", { buffer = true })
