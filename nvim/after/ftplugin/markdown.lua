vim.opt_local.wrap = true
vim.opt_local.linebreak = true
vim.opt_local.spell = true

vim.keymap.set("n", "j", "gj", { buffer = true })
vim.keymap.set("n", "k", "gk", { buffer = true })
vim.keymap.set("v", "j", "gj", { buffer = true })
vim.keymap.set("v", "k", "gk", { buffer = true })

vim.keymap.set("v", "|", ":EasyAlign*|<cr>", { buffer = true })

-- Convert md to word and store in standard location
vim.api.nvim_create_user_command('Pandoc', function()
    local currentDir = vim.fn.getcwd()
    local fileName = vim.fn.expand("%:t:r")
    local fileDir = vim.fn.expand("%:h")
    local pandocDir = "~/pandoc_word/"
    vim.cmd("cd " .. fileDir)
    vim.cmd("! pandoc -o " .. pandocDir .. fileName .. ".docx" .. " -f markdown -t docx %")
    vim.cmd("cd " .. currentDir)
end, {})
