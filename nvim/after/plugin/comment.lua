-- Guard against not installed
local status_ok, _ = pcall(require, "Comment")
if not status_ok then
    return
end

local comment = require('Comment')

comment.setup({
    padding = true,
    sticky = true,
    ignore = nil,
    toggler = {
        line = '<leader>/',
        block = 'gbc',
    },
    opleader = {
        line = '<leader>/',
        block = 'gb',
    },
    extra = {
        above = 'gcO',
        below = 'gco',
        eol = 'gcA',
    },
    mappings = {
        basic = true,
        extra = true,
    },
    pre_hook = nil,
    post_hook = nil,
})
