-- Guard against not installed
local status_ok, _ = pcall(require, "nvim-web-devicons")
if not status_ok then
    return
end

require('nvim-web-devicons').set_icon {
    csv = {
        icon = "",
        color = "#ffffff",
        cterm_color = "231",
        name = "Csv"
    },
    txt = {
        icon = "",
        color = "#ffffff",
        cterm_color = "231",
        name = "Text"
    }
}
