-- Guard against not installed
local as_status_ok, _ = pcall(require, "auto-session")
if not as_status_ok and _ then
    return
end

local nvimtree_status_ok, _ = pcall(require, "nvim-tree")
local packer_status_ok, _ = pcall(require, 'packer')
local tagbar_ok

if packer_status_ok then
    require('packer')
    if packer_plugins["tagbar"] then
        tagbar_ok = true
    else
        tagbar_ok = false
    end
end

local psc_opts

-- Close buffers which cause issues if tried to load at launch
if nvimtree_status_ok and tagbar_ok then
    psc_opts = { "NvimTreeClose", "TagbarClose", "RemoveBashREPL", "RemovePyREPL" }
elseif nvimtree_status_ok then
    psc_opts = { "NvimTreeClose", "RemoveBashREPL", "RemovePyREPL" }
elseif tagbar_ok then
    psc_opts = { "TagbarClose", "RemoveBashREPL", "RemovePyREPL" }
end

require("auto-session").setup {
    pre_save_cmds = psc_opts
}
