-- Guard against not installed
local lsp_zero_status_ok, _ = pcall(require, "lsp-zero")
local mason_status_ok, _ = pcall(require, "mason")
local mason_config_status_ok, _ = pcall(require, "mason-lspconfig")
if not lsp_zero_status_ok and mason_status_ok and mason_config_status_ok then
    return
end

require("mason").setup()
require("mason-lspconfig").setup({
    ensure_installed = {
        "clangd",
        "cmake",
        "lua_ls",
        "marksman",
        "matlab_ls",
        "pylsp",
    }
})

local lsp = require('lsp-zero').preset({
    manage_nvim_cmp = {
        set_basic_mappings = true,
    }
})

lsp.set_sign_icons({
    error = '',
    warn = '',
    hint = '',
    info = ''
})

lsp.on_attach(function(client, bufnr)
    lsp.default_keymaps({ buffer = bufnr })
end)

-- (Optional) Configure lua language server for neovim
require('lspconfig').lua_ls.setup(lsp.nvim_lua_ls())
require('lspconfig').matlab_ls.setup {
    settings = {
        matlab = {
            installPath = "/home/scott/R2021b",
            matlabConnectionTiming = "onStart",
            telemetry = false,
        }
    }
}

lsp.format_on_save({
    format_opts = {
        async = false,
        timeout_ms = 10000,
    },
    servers = {
        ['clangd'] = { 'cpp' },
        ['lua_ls'] = { 'lua' },
        ['marksman'] = { 'markdown' },
        ['pylsp'] = { 'python' },
    }
})
lsp.setup()

require 'lspconfig'['pylsp'].setup {
    on_attach = on_attach,
    filetypes = { 'python' },
    settings = {
        configurationSources = {},
        formatCommand = {},
        pylsp = {
            plugins = {
                jedi_completion = {
                    enabled = true,
                    fuzzy = true,
                    include_class_objects = true,
                    include_function_objects = true,
                    include_params = true,
                },
                pycodestyle = {
                    enabled = true,
                    ignore = { 'E501', 'E231' },
                    maxLineLength = 120,
                },
                pylint = {
                    args = { "--unsafe-load-any-extension=y" },
                    enabled = true,
                },
            }
        }
    }
}

require('lspconfig').clangd.setup {
    cmd = {
        os.getenv("HOME") .. "/.local/share/nvim/mason/bin/clangd",
        "--header-insertion-decorators=false",
        "--header-insertion=never"
    }

}
