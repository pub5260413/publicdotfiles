local status_ok, _ = pcall(require, 'packer')
if status_ok then
    if packer_plugins["tagbar"] then
        vim.g.tagbar_position = 'topleft vertical'
        vim.g.tagbar_map_nexttag = ''
        vim.g.tagbar_compact = 1
        vim.g.tagbar_width = 40
        vim.g.tagbar_ctags_bin = '/usr/bin/ctags-universal'
        vim.g.tagbar_silent = 1
        vim.g.tagbar_sort = 0
    end
end
