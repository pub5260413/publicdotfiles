-- Guard against not installed
local status_ok, _ = pcall(require, "lualine")
if not status_ok then
    return
end

local utils = require('scott.utils')

-- See lualine bug #799
local function diff_source()
    local gitsigns = vim.b.gitsigns_status_dict
    if gitsigns then
        return {
            added = gitsigns.added,
            modified = gitsigns.changed,
            removed = gitsigns.removed
        }
    end
end

-- get current project name
local function project_name()
    if vim.o.columns > 72 then
        return vim.fn.fnamemodify(vim.fn.getcwd(), ':t')
    else
        return ''
    end
end

-- Get current branch name
local function branch_name()
    local branch = vim.fn.system("git branch --show-current 2> /dev/null | tr -d '\n'")
    if branch ~= "" then
        return branch
    else
        return ""
    end
end

vim.api.nvim_create_augroup("lualine_git", { clear = true })
vim.api.nvim_create_autocmd({ "VimEnter", "BufEnter", "FocusGained" }, {
    callback = function()
        vim.b.branch_name = branch_name()
        vim.b.project_name = project_name()
    end,
    group = "lualine_git"
})

local theme = {
    normal = {
        a = { fg = utils.palette.black, bg = utils.palette.blue },
        b = { fg = utils.palette.white, bg = utils.palette.bg_dim },
        c = { fg = utils.palette.black, bg = utils.palette.bg_dim },
    },

    insert = { a = { fg = utils.palette.black, bg = utils.palette.green } },
    visual = { a = { fg = utils.palette.black, bg = utils.palette.orange } },
    replace = { a = { fg = utils.palette.black, bg = utils.palette.orange } },
    command = { a = { fg = utils.palette.black, bg = utils.palette.yellow } },
    terminal = { a = { fg = utils.palette.black, bg = utils.palette.purple } },

    inactive = {
        a = { fg = utils.palette.grey, bg = utils.palette.bg_dim },
        b = { fg = utils.palette.white, bg = utils.palette.bg_dim },
        c = { fg = utils.palette.black, bg = utils.palette.bg_dim },
    },
}

require('lualine').setup {
    options = {
        icons_enabled = true,
        theme = theme,
        component_separators = { left = '', right = '' },
        section_separators = { left = '', right = '' },
        disabled_filetypes = {
            winbar = {
                'NvimTree',
                'tagbar',
            },
        },
        ignore_focus = {},
        always_divide_middle = true,
        globalstatus = true,
        refresh = {
            statusline = 1000,
            tabline = 1000,
            winbar = 1000,
        }
    },
    sections = {
        lualine_a = { 'mode' },
        lualine_b = {
            { 'b:project_name' },
            { 'b:branch_name' },
            {
                'diff',
                colored = true,
                source = diff_source,
            },
            {
                'diagnostics',
                sections = { 'error', 'warn', 'info', 'hint' },
                symbols = { error = ' ', warn = ' ', info = ' ', hint = ' ' },
            }
        },
        lualine_c = {
        },
        lualine_x = {},
        lualine_y = { 'encoding', 'fileformat', 'filetype' },
        lualine_z = {}
    },
    inactive_sections = {
        lualine_a = {},
        lualine_b = {},
        lualine_c = { 'filename' },
        lualine_x = { 'location' },
        lualine_y = {},
        lualine_z = {}
    },
    tabline = {
        lualine_a = { {
            'buffers',
            use_mode_colors = true,
            filetype_names = {
                NvimTree = 'NvimTree',
                tagbar = 'tagbar',
            },
        } },
        lualine_y = { 'progress' },
        lualine_z = { 'location' }
    },
    winbar = {},
    inactive_winbar = {
        lualine_z = { 'filename' }
    },
    extensions = {}
}
