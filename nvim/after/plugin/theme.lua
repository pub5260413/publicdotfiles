local utils = require('scott.utils')

vim.cmd.colorscheme("habamax")

-- Adjust colorscheme
vim.api.nvim_set_hl(0, 'NormalNC', { bg = 'black' })
vim.api.nvim_set_hl(0, 'MsgArea', { link = 'NormalNC' })
vim.api.nvim_set_hl(0, 'VertSplit', { link = 'Normal' })
vim.api.nvim_set_hl(0, 'DiagnosticError', { fg = utils.palette.red })
vim.api.nvim_set_hl(0, 'DiagnosticFloatingError', { fg = utils.palette.red })
vim.api.nvim_set_hl(0, 'DiagnosticWarn', { fg = utils.palette.orange })
vim.api.nvim_set_hl(0, 'DiagnosticFloatingWarn', { fg = utils.palette.orange })
vim.api.nvim_set_hl(0, 'DiffChange', { fg = utils.palette.blue })
vim.api.nvim_set_hl(0, 'DiffAdd', { fg = utils.palette.green })
vim.api.nvim_set_hl(0, 'DiffDelete', { fg = utils.palette.red })
vim.api.nvim_set_hl(0, 'Function', { fg = utils.palette.dark_red })
vim.api.nvim_set_hl(0, 'MatchParen', { fg = utils.palette.orange })
vim.api.nvim_set_hl(0, 'LspSignatureActiveParameter', { fg = 'black', bg = utils.palette.orange })
vim.api.nvim_set_hl(0, '@markup', { fg = utils.palette.yellow })
vim.api.nvim_set_hl(0, '@constructor.cpp', { link = 'Function' })

-- Disable lsp highlights - works better w/ simple themes
for _, group in ipairs(vim.fn.getcompletion("@lsp", "highlight")) do
    vim.api.nvim_set_hl(0, group, {})
end
