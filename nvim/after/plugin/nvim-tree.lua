-- Guard against not installed
local status_ok, _ = pcall(require, "nvim-tree")
if not status_ok then
    return
end

-- disable netrw at the very start of your init.lua
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

require("nvim-tree").setup({
    view = {
        width = 40,
        signcolumn = "no",
    },
    filters = {
        git_ignored = false,
    },
})

local api = require("nvim-tree.api")
local Event = api.events.Event

-- Skeleton files created with nvim-tree
api.events.subscribe(Event.FileCreated,
    function(data)
        vim.cmd("edit " .. data.fname)
        if string.match(data.fname, "%.py$") then vim.cmd("0r ~/.config/nvim/template/skeleton.py") end
        if string.match(data.fname, "%.sh$") then vim.cmd("0r ~/.config/nvim/template/skeleton.sh") end
        if string.match(data.fname, "%.clang%-format$") then vim.cmd("0r ~/.config/nvim/template/skeleton.clang-format") end
        if string.match(data.fname, "%.clangd$") then vim.cmd("0r ~/.config/nvim/template/skeleton.clangd") end
        if string.match(data.fname, "%.cpp$") and string.match(data.fname, "/python/") then
            vim.cmd(
                "0r ~/.config/nvim/template/skeleton.pybind")
        end
        if string.match(data.fname, "CMakeLists.txt") then
            if string.match(data.fname, "/src/") then
                vim.cmd("0r ~/.config/nvim/template/skeleton.cmake-src")
            elseif string.match(data.fname, "/tests/") then
                vim.cmd("0r ~/.config/nvim/template/skeleton.cmake-tests")
            elseif string.match(data.fname, "/python/") then
                vim.cmd("0r ~/.config/nvim/template/skeleton.cmake-pybind")
            else
                vim.cmd("0r ~/.config/nvim/template/skeleton.cmake-root")
            end
        end
    end)

api.events.subscribe(Event.FileRemoved, function(data) api.tree.reload() end)

-- Adjust habamax colors **NormalNC needs to be redefined here for race duing boot
api.events
    .subscribe(api.events.Event.TreeOpen, function()
        vim.api.nvim_set_hl(0, 'NormalNC', { bg = 'black' })
        vim.api.nvim_set_hl(0, 'NvimTreeNormal', { link = 'Normal' })
        vim.api.nvim_set_hl(0, 'NvimTreeNormalNC', { link = 'NormalNC' })
        vim.api.nvim_set_hl(0, 'NvimTreeEndOfBuffer', { link = 'EndOfBuffer' })
        vim.api.nvim_set_hl(0, 'NvimTreeNormal', { link = 'SignColumn' })
        vim.api.nvim_set_hl(0, 'NvimTreeCursorLine', { link = 'CursorLine' })

        vim.wo.fillchars = 'eob: '
        vim.wo.statuscolumn = ''
    end)
