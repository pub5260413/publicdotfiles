-- nvimtree toggle and code outline
-- Guard against not installed
local nvimtree_status_ok, _ = pcall(require, "nvim-tree")
local packer_status_ok, _ = pcall(require, 'packer')
local lualine_status_ok, _ = pcall(require, "lualine")
local tagbar_ok
local gitsigns_status_ok, _ = pcall(require, "gitsigns")
local utils = require('scott.utils')

if packer_status_ok then
    require('packer')
    if packer_plugins["tagbar"] then
        tagbar_ok = true
    else
        tagbar_ok = false
    end
end

-- Only open one sidebar at a time
if nvimtree_status_ok and tagbar_ok then
    vim.api.nvim_set_keymap("n", "<C-n>", "", {
        callback = function()
            vim.cmd("TagbarClose")
            vim.cmd("NvimTreeRefresh")
            vim.cmd("NvimTreeToggle")
        end,
        silent = true,
        noremap = true
    })
    vim.api.nvim_set_keymap("n", "<C-b>", "", {
        callback = function()
            vim.cmd("NvimTreeClose")
            vim.cmd("TagbarToggle f")
            vim.cmd("wincmd =")
        end,
        silent = true,
        noremap = true
    })
elseif nvimtree_status_ok then
    vim.api.nvim_set_keymap("n", "<C-n>", "", {
        callback = function()
            vim.cmd("NvimTreeRefresh")
            vim.cmd("NvimTreeToggle")
        end,
        silent = true,
        noremap = true
    })
elseif tagbar_ok then
    vim.api.nvim_set_keymap("n", "<C-b>", "", {
        callback = function()
            vim.cmd("TagbarToggle f")
            vim.cmd("wincmd =")
        end,
        silent = true,
        noremap = true
    })
end

-- Lualine buffer jump hotkeys and buffer delete autocommand
if lualine_status_ok then
    local function LuaLineTabKeymap()
        local bufNumb = vim.fn.len(vim.fn.getbufinfo({ buflisted = 1 }))
        if bufNumb >= 1 then
            vim.api.nvim_set_keymap("n", "<leader>1", ":LualineBuffersJump 1<cr>", { silent = true, noremap = true })
            vim.api.nvim_set_keymap("n", "<leader>2", "", { silent = true, noremap = true })
            vim.api.nvim_set_keymap("n", "<leader>3", "", { silent = true, noremap = true })
            vim.api.nvim_set_keymap("n", "<leader>4", "", { silent = true, noremap = true })
            vim.api.nvim_set_keymap("n", "<leader>5", "", { silent = true, noremap = true })
            vim.api.nvim_set_keymap("n", "<leader>6", "", { silent = true, noremap = true })
            vim.api.nvim_set_keymap("n", "<leader>7", "", { silent = true, noremap = true })
        end
        if bufNumb >= 2 then
            vim.api.nvim_set_keymap("n", "<leader>2", ":LualineBuffersJump 2<cr>", { silent = true, noremap = true })
            vim.api.nvim_set_keymap("n", "<leader>3", "", { silent = true, noremap = true })
            vim.api.nvim_set_keymap("n", "<leader>4", "", { silent = true, noremap = true })
            vim.api.nvim_set_keymap("n", "<leader>5", "", { silent = true, noremap = true })
            vim.api.nvim_set_keymap("n", "<leader>6", "", { silent = true, noremap = true })
            vim.api.nvim_set_keymap("n", "<leader>7", "", { silent = true, noremap = true })
        end
        if bufNumb >= 3 then
            vim.api.nvim_set_keymap("n", "<leader>3", ":LualineBuffersJump 3<cr>", { silent = true, noremap = true })
            vim.api.nvim_set_keymap("n", "<leader>4", "", { silent = true, noremap = true })
            vim.api.nvim_set_keymap("n", "<leader>5", "", { silent = true, noremap = true })
            vim.api.nvim_set_keymap("n", "<leader>6", "", { silent = true, noremap = true })
            vim.api.nvim_set_keymap("n", "<leader>7", "", { silent = true, noremap = true })
        end
        if bufNumb >= 4 then
            vim.api.nvim_set_keymap("n", "<leader>4", ":LualineBuffersJump 4<cr>", { silent = true, noremap = true })
            vim.api.nvim_set_keymap("n", "<leader>5", "", { silent = true, noremap = true })
            vim.api.nvim_set_keymap("n", "<leader>6", "", { silent = true, noremap = true })
            vim.api.nvim_set_keymap("n", "<leader>7", "", { silent = true, noremap = true })
        end
        if bufNumb >= 5 then
            vim.api.nvim_set_keymap("n", "<leader>5", ":LualineBuffersJump 5<cr>", { silent = true, noremap = true })
            vim.api.nvim_set_keymap("n", "<leader>6", "", { silent = true, noremap = true })
            vim.api.nvim_set_keymap("n", "<leader>7", "", { silent = true, noremap = true })
        end
        if bufNumb >= 6 then
            vim.api.nvim_set_keymap("n", "<leader>6", ":LualineBuffersJump 6<cr>", { silent = true, noremap = true })
            vim.api.nvim_set_keymap("n", "<leader>7", "", { silent = true, noremap = true })
        end
        if bufNumb >= 7 then
            vim.api.nvim_set_keymap("n", "<leader>7", ":LualineBuffersJump 7<cr>", { silent = true, noremap = true })
        end
    end

    vim.api.nvim_set_keymap("n", "<leader>d", '', {
        callback = function()
            vim.cmd('bp|bd#')
            LuaLineTabKeymap()
        end,
        silent = true,
        noremap = true
    })

    vim.api.nvim_create_augroup("LuaLineNumber", { clear = true })
    vim.api.nvim_create_autocmd({ "BufAdd", "BufWinEnter", "VimEnter", "CmdlineLeave", "BufEnter" }, {
        callback = function()
            LuaLineTabKeymap()
        end,
        group = "LuaLineNumber"
    })
end

if gitsigns_status_ok then
    vim.api.nvim_set_keymap("n", "<leader>'", '<cmd>lua require"gitsigns".next_hunk()<CR>',
        { silent = true, noremap = true })
end

local function lowBudgetFloaterm(fileName, cmd)
    local fileType = vim.bo.filetype
    if fileType == "NvimTree" or fileType == "tagbar" then
        return
    end
    local bufWinNum = #vim.api.nvim_tabpage_list_wins(0)
    utils.NewBufferAtPosition("beta")
    if vim.fn.bufexists(fileName) ~= 0 then
        vim.cmd("b " .. fileName)
        vim.api.nvim_feedkeys(
            vim.api.nvim_replace_termcodes("i", true, false, true)
            , 'x!', true)
    else
        vim.g.lastBuffer = vim.fn.bufnr()
        if bufWinNum == 1 or
            (bufWinNum == 2 and
                (vim.fn['tagbar#IsOpen']() == 1 or
                    require("nvim-tree.api").tree.winid() ~= nil)) then
            vim.g.lastWinState = 1
        else
            vim.g.lastWinState = 2
        end
        vim.cmd("term")
        vim.cmd("file " .. fileName)
        vim.bo.filetype = "REPL"
        vim.api.nvim_feedkeys(
            vim.api.nvim_replace_termcodes(cmd, true, false, true)
            , 'x!', true)
    end
end

-- Low budget floaterm python
vim.keymap.set("n", "<leader>py", function()
    lowBudgetFloaterm("pyREPL", "ipython3 -i ~/.dotfiles/python/startupImports.py<cr><C-l>")
end, { silent = true, noremap = true })

-- Remove pyREPL
vim.api.nvim_create_user_command('RemovePyREPL', function()
    if vim.fn.bufloaded('pyREPL') == 1 then
        vim.cmd('bd! pyREPL')
    end
end, {})

-- Low budget floaterm bash
vim.keymap.set("n", "<leader>c", function()
    lowBudgetFloaterm("bashREPL", "i")
end, { silent = true, noremap = true })

-- Remove bashREPL
vim.api.nvim_create_user_command('RemoveBashREPL', function()
    if vim.fn.bufloaded('bashREPL') == 1 then
        vim.cmd('bd! bashREPL')
    end
end, {})
