-- Guard against not installed
local status_ok, _ = pcall(require, "cmp")
if not status_ok then
    return
end

local cmp = require('cmp')

require('lsp-zero').extend_cmp()
local cmp_action = require('lsp-zero').cmp_action()

cmp.setup({
    formatting = {
        format = function(entry, vim_item)
            vim_item.abbr = string.sub(vim_item.abbr, 1, 20)
            if vim_item.menu then
                if string.len(vim_item.menu) > 13 then
                    vim_item.menu = string.sub(vim_item.menu, 1, 9) .. ".."
                else
                    vim_item.menu = string.sub(vim_item.menu, 1, 13)
                end
            end
            return vim_item
        end
    },
    window = {
        documentation = false,
    },
    mapping = {
        ['<C-f>'] = cmp_action.luasnip_jump_forward(),
        ['<C-k>'] = function()
            cmp.close()
            vim.lsp.buf.signature_help()
        end,
    },
    performance = {
        max_view_entries = 8,
    },
    preselect = 'item',
    completion = {
        completeopt = 'menu,menuone,noinsert'
    },
    sources = {
        { name = "nvim_lsp" },
        { name = "buffer",  max_item_count = 5, keyword_length = 3 },
    },
    experimental = {
        ghost_text = false,
        native_menu = false,
    },
    cmp.setup.cmdline({ '/', '?' }, {
        -- autocomplete = false,
        mapping = cmp.mapping.preset.cmdline(),
        sources = {
            { name = 'buffer', max_item_count = 5 }
        }
    }),
    cmp.setup.cmdline({ ':' }, {
        mapping = cmp.mapping.preset.cmdline(),
        sources = {
            { name = 'buffer',  max_item_count = 5 },
            { name = 'path',    max_item_count = 5 },
            { name = 'cmdline', max_item_count = 5 }
        },
        {
            name = 'cmdline',
        }
    }),
})
