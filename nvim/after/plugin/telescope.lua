-- Guard against not installed
local status_ok, _ = pcall(require, "telescope")
if not status_ok then
    return
end

require('telescope').setup {
    defaults = {
        mappings = {
            i = {
                ["<C-c>"] = false,
            },
        }
    }
}

local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>ff', builtin.find_files, { silent = true, noremap = true })
vim.keymap.set('n', '<leader>gf', function() builtin.git_files({ recurse_submodules = true }) end,
    { silent = true, noremap = true })
local notesDir = os.getenv("SCOTT_NOTES")
if notesDir then
    vim.keymap.set('n', '<leader>nf', ":Telescope live_grep cwd=" .. notesDir .. "<cr>",
        { silent = true, noremap = true })
end
