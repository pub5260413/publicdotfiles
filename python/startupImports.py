#!/usr/bin/python3

import numpy as np
import pandas as pd
import pyperclip
import visidata
import plotext as plt
float = np.float32
single = np.float32
plt.clc()
def cp(a): pyperclip.copy(str(a))
def view(a): visidata.vd.view_pandas(a)
